import os.path
import warnings

import ruyaml

import cookiecutter.main

INFINITE_LOOP = False

# Unfortunately, it turns out that replay.yaml is read before pre_gen_project.py is run.

def read_project_branches_to_trigger():
  yaml = ruyaml.YAML()
  with open('project_branches_to_trigger.yaml', 'r') as triggerFile:
    project_branches_to_trigger = yaml.load(triggerFile)
  if type(project_branches_to_trigger) is not ruyaml.comments.CommentedMap:
    raise TypeError(type(project_branches_to_trigger), project_branches_to_trigger)
  if 'project_branches_to_trigger' not in project_branches_to_trigger:
    raise ValueError(project_branches_to_trigger)
  return project_branches_to_trigger

def read_replay_yaml(replay_yaml_path='replay.yaml'):
  yaml = ruyaml.YAML()
  with open(replay_yaml_path, 'r') as replayFile:
    replay = yaml.load(replayFile)
  if 'cookiecutter' not in replay:
    raise ValueError(replay)
  cookiecutter = replay['cookiecutter']
  if type(cookiecutter) is not ruyaml.comments.CommentedMap:
    raise TypeError(type(cookiecutter), cookiecutter)
  if 'project_branches_to_trigger' not in cookiecutter:
    raise ValueError(cookiecutter)
  return replay

def edit_replay_yaml_to_match_project_branches_to_trigger_yaml():
  if not os.path.exists('replay.yaml'):
    warnings.warn('replay.yaml not found. Hopefully you are cookiecutting a fresh includable.')
  elif os.path.exists('project_branches_to_trigger.yaml'):
    replay = read_replay_yaml('replay.yaml')
    project_branches_to_trigger = read_project_branches_to_trigger()['project_branches_to_trigger']
    if replay['cookiecutter']['project_branches_to_trigger'] != project_branches_to_trigger:
      warnings.warn('project_branches_to_trigger.yaml differs from project_branches_to_trigger in replay.yaml, so we are rewriting replay.yaml to match project_branches_to_trigger.yaml.')
      replay['cookiecutter']['project_branches_to_trigger'] = project_branches_to_trigger
      yaml = ruyaml.YAML()
      with open('replay.yaml', 'w') as replayFile:
        yaml.dump(replay, replayFile)
      replay = read_replay_yaml('replay.yaml')
      assert replay['cookiecutter']['project_branches_to_trigger'] == project_branches_to_trigger
      assert not INFINITE_LOOP
      # cookiecutter.main.cookiecutter('..', replay='replay.yaml', overwrite_if_exists=True)
    assert replay['cookiecutter']['project_branches_to_trigger'] == project_branches_to_trigger
  else:
    warnings.warn('project_branches_to_trigger.yaml not found. Hopefully you are cookiecutting a fresh includable.')


edit_replay_yaml_to_match_project_branches_to_trigger_yaml()
