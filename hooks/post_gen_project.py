#!/usr/bin/env python

import os

for dirpath, dirs, files in os.walk(os.getcwd()):
    if 'keep' in files:
        for file in files:
            path = os.path.join(dirpath, file)
            os.unlink(path)

import os
import warnings

import json
import ruyaml

import cookiecutter.main

def read_project_branches_to_trigger():
  yaml = ruyaml.YAML()
  with open('project_branches_to_trigger.yaml', 'r') as triggerFile:
    project_branches_to_trigger = yaml.load(triggerFile)
  if type(project_branches_to_trigger) is not ruyaml.comments.CommentedMap:
    raise TypeError(type(project_branches_to_trigger), project_branches_to_trigger)
  if 'project_branches_to_trigger' not in project_branches_to_trigger:
    raise ValueError(project_branches_to_trigger)
  return project_branches_to_trigger

def read_replay_yaml(replay_yaml_path='replay.yaml'):
  yaml = ruyaml.YAML()
  with open(replay_yaml_path, 'r') as replayFile:
    replay = yaml.load(replayFile)
  if 'cookiecutter' not in replay:
    raise ValueError(replay)
  cookiecutter = replay['cookiecutter']
  if type(cookiecutter) is not ruyaml.comments.CommentedMap:
    raise TypeError(type(cookiecutter), cookiecutter)
  if 'project_branches_to_trigger' not in cookiecutter:
    raise ValueError(cookiecutter)
  return replay

def edit_replay_yaml_to_match_project_branches_to_trigger_yaml():
  if not os.path.exists('replay.yaml'):
    warnings.warn('replay.yaml not found. Hopefully you are cookiecutting a fresh includable.')
  elif os.path.exists('project_branches_to_trigger.yaml'):
    replay = read_replay_yaml('replay.yaml')
    project_branches_to_trigger = read_project_branches_to_trigger()['project_branches_to_trigger']
    if replay['cookiecutter']['project_branches_to_trigger'] != project_branches_to_trigger:
      warnings.warn('project_branches_to_trigger.yaml differs from project_branches_to_trigger in replay.yaml, so we are rewriting replay.yaml to match project_branches_to_trigger.yaml.')
      replay['cookiecutter']['project_branches_to_trigger'] = project_branches_to_trigger
      yaml = ruyaml.YAML()
      with open('replay.yaml', 'w') as replayFile:
        yaml.dump(replay, replayFile)
      replay = read_replay_yaml('replay.yaml')
    assert replay['cookiecutter']['project_branches_to_trigger'] == project_branches_to_trigger
  else:
    warnings.warn('project_branches_to_trigger.yaml not found. Hopefully you are cookiecutting a fresh includable.')

def create_replay_yaml():
  if os.path.exists('replay.yaml'):
    return
  replayJSONpath = os.path.join(os.path.expanduser('~'), '.cookiecutter_replay', 'trigger-pipelines-cookiecutter.json')
  if not os.path.exists(replayJSONpath):
    raise Exception(replayJSONpath, os.listdir(os.path.dirname(replayJSONpath)))
  with open(replayJSONpath) as replayJSONfile:
    replay = json.load(replayJSONfile)
  yaml = ruyaml.YAML()
  if 'project_branches_to_trigger' not in replay['cookiecutter']:
    replay['cookiecutter']['project_branches_to_trigger'] = list()
  with open('replay.yaml', 'w') as replayFile:
    yaml.dump(replay, replayFile)

def create_project_branches_to_trigger_yaml_from_replay_yaml():
  if os.path.exists('project_branches_to_trigger.yaml'):
    # If it already exists, don't mess with it.
    return
  assert os.path.exists('replay.yaml')
  replay = read_replay_yaml('replay.yaml')
  to_write = dict()
  to_write['project_branches_to_trigger'] = replay['cookiecutter']['project_branches_to_trigger']
  yaml = ruyaml.YAML()
  if 'OVERRIDE_PROJECT_BRANCHES_TO_TRIGGER' in os.environ:
    if not os.path.exists('../project_branches_to_trigger.yaml'):
      raise Exception('OVERRIDE_PROJECT_BRANCHES_TO_TRIGGER is set, but no override project_branches_to_trigger.yaml found.')
    with open('../project_branches_to_trigger.yaml', 'r') as triggerFile:
      to_write = yaml.load(triggerFile)
  with open('project_branches_to_trigger.yaml', 'w') as triggerFile:
    yaml.dump(to_write, triggerFile)

DEFAULT_GITLAB_CI_YML = '''
include:
- local: 'trigger-pipelines.yml'

stages:
- trigger
- propagate_changes
'''

def default_gitlab_ci_yml():
  if os.path.exists('.gitlab-ci.yml'):
    return
  with open('.gitlab-ci.yml', 'w') as CIfile:
    CIfile.write(DEFAULT_GITLAB_CI_YML)

create_replay_yaml()
create_project_branches_to_trigger_yaml_from_replay_yaml()
default_gitlab_ci_yml()
